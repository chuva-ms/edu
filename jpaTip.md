## jpa

@NotNull = Null만 허용하지 않는다.
따라서, "" 이나 " " 은 허용.

@NotEmpty = Null, "" 허용하지 않는다.

@NotBlank = Null, "", " " 모두 허용하지 않는다.